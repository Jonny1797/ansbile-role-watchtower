Mailrise Ansible Role
=========

This role installs [mailrise](https://github.com/YoRyan/mailrise) to run as a [Docker](https://www.docker.com/) container wrapped in a systemd service.

Requirements
------------

Must be run on a system with systemd and with Docker installed.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

This role implicitly depends on:

- [`com.devture.ansible.role.playbook_help`](https://github.com/devture/com.devture.ansible.role.playbook_help)
- [`com.devture.ansible.role.systemd_docker_base`](https://github.com/devture/com.devture.ansible.role.systemd_docker_base)

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - role: galaxy/com.devture.ansible.role.systemd_docker_base

        - name: "mailrise"
          src: "https://gitlab.com/Jonny1797/ansbile-role-watchtower.git"

        - role: another_role

Example playbook configuration (`group_vars/servers` or other):  

    mailrise_enabled: false

    mailrise_identifier: my-mailrise

    mailrise_base_path: "{{ my_base_path }}/mailrise"

    mailrise_username: "{{ my_username }}"


A user must set a configuration like this in their `vars.yml`

    mailrise_enabled: true
    mailrise_ntfy_enabled: true
    mailrise_ntfy_token: 'my_secret_token'
    mailrise_ntfy_url: 'ntfy.mydomain.tld'
    mailrise_ntfy_topic: 'mailrise'

For the full list of supported customization options, check [defaults/main.yml](defaults/main.yml).

License
-------

GPL 3.0
